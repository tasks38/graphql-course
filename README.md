# Project Title

GraphQL course


## Run Locally

Clone the project

```bash
  git clone git@gitlab.com:tasks38/graphql-course.git
```

Go to the project directory

```bash
  cd graphql-course
```

Install dependencies

```bash
  npm install --global nodemon
  npm install
```

Start the server

```bash
  nodemon index
```
